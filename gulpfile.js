
const elixir = require('laravel-elixir');

var argv = require('yargs').argv;
var del = require('del');

require('laravel-elixir-vue-2');
require('laravel-elixir-webpack-official');

var host;

if (argv.host)
    host = argv.host;

elixir.webpack.mergeConfig({
    babel: {
        presets: ['es2015']
    },
    module: {
        preLoaders: [{
            test: /\.json$/,
            loader: 'json'
        }],
        loaders:[
            { test: /\.css$/, loader: 'style!css!' },
        ]
    }
});

elixir((mix) => {
    mix
        .webpack(
            [
                'resources/assets/js/app.js'
            ],
            'public/js/all.min.js'
        )
        .sass(
            [
                'resources/assets/sass/app.scss'
            ],
            'public/css/all.min.css'
        )
        .browserSync({
           online: false,
           proxy: host,
        });
});

gulp.task('clean', function() {
    return del.sync(['public/js', 'public/css']);
});

