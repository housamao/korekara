var app = new Vue({
	el: '#app',
	mounted() {
	},
	data: function() {
		return {
			lat: 0,
			lng: 0,
			urlApi: 'https://api.openweathermap.org/data/2.5/',
			corsAnywhere: 'https://cors-anywhere.herokuapp.com/',
			apiKey: 'ca1917eac253a53f03716b74dde38506',
			waiting: false,
			error: '',
			dataWeathers: [{
				thumbnail: '',
				date: '',
				type: '',
				max: '',
				min: '',
			}],
			city:'',
		}
	},
	methods: {
		getCurrentLocation() {
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(this.showPosition);
			} else {
				this.error = "Geolocation is not supported by this browser.";
			}
		},

		showPosition(position) {
			this.lat = Math.round(position.coords.latitude);
			this.lng = Math.round(position.coords.longitude);
			this.newLocation('current');
		},

		newLocation(type) {
			this.error = '';
			if(type == "zip-code") {
				console.log('zip-code')
				var inputZip = document.getElementsByClassName('input-zip')[0];
				var url = this.corsAnywhere+this.urlApi+'forecast?zip='+inputZip.value+',jp&appid='+this.apiKey;
			} else {
				console.log('other')
				var url = this.corsAnywhere+this.urlApi+'forecast?lat='+this.lat+'&lon='+this.lng+'&appid='+this.apiKey;
			}

			this.waiting = true;
			this.$http.get(this.corsAnywhere+"http://api.zippopotam.us/JP/"+inputZip.value).then(response => {
				this.city = response.body.places[0]["place name"]+", "+ response.body.places[0]["state"];
			}, response => {
				this.error = "Please enter a right zip-code!";
			});
			this.$http.get(url).then(response => {
				var tabTemp = response.body.list;
				var self = this;
				var tempData = [];
				var i = 0;
				tabTemp.forEach(function(list, index) {
					self.waiting = false;

					if(index == 0) {
						tempData.push( list );
						self.$set(self.dataWeathers, index, list);

					}

					if(list.dt_txt.substr(0, 10) !== tempData[i].dt_txt.substr(0, 10) && i !== 2) {
						tempData.push(list);
						i++;
						self.$set(self.dataWeathers, i, list);
					}
				});

			}, response => {
				this.error = "Please enter a right zip-code!";
				this.waiting = false;
			});
		},
	}
});