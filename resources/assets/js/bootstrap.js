window._ = require('lodash');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.Vue = require('vue');

window.$ = window.jQuery = require('jquery');

Vue.use(require('vue-resource'));

Vue.http.options.method = 'GET, PUT, POST, DELETE, HEAD, OPTIONS';
Vue.http.headers.common['Access-Control-Allow-Origin'] = true;

Vue.http.interceptors.push((request, next) => {
	request.headers.set('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
	request.headers.set('Authorization', 'basic ' + $('meta[name="csrf-token"]').attr('content'));
	request.headers.set('Access-Control-Allow-Origin', '*');
	request.headers.set('Access-Control-Allow-Methods', '*');
	request.headers.set('Access-Control-Allow-Credentials', 'true');
	next();
});

Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

Vue.component('weathers', require('./vue/WeatherHistory.vue'));

require('./map');