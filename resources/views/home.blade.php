<!DOCTYPE html>
<html lang="en-EN">
<head>
	<meta charset='utf-8'>
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1">
	<meta name="description" content="technical test">

	<!-- CSRF Token -->
	<meta name='csrf-token' content="{{ csrf_token() }}">

	<!-- TITLE -->
	<title>Korekara - TEST</title>

	<!-- Styles -->
	<link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/font-awesome/latest/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />

	<script type="text/javascript">
		window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
		]) !!};
	</script>
</head>
<body>
	
	<div id="app">
		<div class="wrapper wrapper-head">
			<div class="container container-head">
				<div>
					<h1 class="title title-main">Corekara</h1>
				</div>
				<div class="container-form">
					<p id="error-geoloc" class="error error-geoloc" v-html="error"></p>
					<input class="input-zip" type="text" name="posta-code" placeholder="postal code 164-0001">
					<input class="btn btn-submit" type="submit" name="submit" v-on:click="newLocation('zip-code')">
					<button class="btn btn-current" v-on:click="getCurrentLocation()"><i class="fa fa-location-arrow"></i> current location</button>
				</div>
			</div>
		</div>
		<div class="wrapper wrapper-main">
			<div class="container container-main">
				<div class="loading" v-show="waiting"></div>
				<div class="city">
					<h3 v-html="city"></h3>
				</div>
				<div class="last-weather" v-if="Array.isArray(dataWeathers) && dataWeathers.length > 1">
					<div class="title title-day">
						<p>3-day forecast</p>
					</div>
					<weathers v-bind:data-weathers="dataWeathers"></weathers>
				</div>
				<div id="map" class="map"></div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-core.js"></script>
	<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-service.js"></script>
	<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-ui.js"></script>
	<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js"></script>
	<script src="{{ asset('js/all.min.js') }}"></script>

</body>
</html>
